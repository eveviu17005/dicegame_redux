import logo from "./logo.svg";
import "./App.css";
import Dice_Layout from "./Dice_Game/Dice_Layout";

function App() {
  return (
    <div className="App">
      <Dice_Layout />
    </div>
  );
}

export default App;
