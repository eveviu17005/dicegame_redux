import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAYER_CHOOSE, PLAY_GAME } from "./redux/constant/constant";

let showResultStyle = {
  fontSize: 30,
};

export class Result extends Component {
  render() {
    return (
      <div>
        <div className="btn btn-success display-4">
          <span onClick={this.props.handlePlayGame} className="display-4">
            Play Game
          </span>
        </div>
        <div style={{ color: "blue" }}>
          <h3>{this.props.result}</h3>
        </div>
        <div className="mt-5">
          <h2>BẠN CHỌN: {this.props.playerChoose}</h2>
          <h4>TỔNG SỐ LẦN CHƠI: {this.props.totalPlay}</h4>
          <h4>TỔNG SỐ LẦN THẮNG: {this.props.totalWin}</h4>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    result: state.diceReducer.result,
    playerChoose: state.diceReducer.playerChoose,
    totalPlay: state.diceReducer.totalPlayCount,
    totalWin: state.diceReducer.totalWin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({
        type: PLAY_GAME,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Result);
